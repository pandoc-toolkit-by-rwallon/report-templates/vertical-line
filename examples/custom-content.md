---
# Section Customization.
numbersections: true
secnumdepth: 4
subparagraph: true

# Miscellaneous.
links-as-notes: true

# Cover and Title Page.
cover: resources/cover.pdf

# Front Matter.
toc: true
toc-depth: 3

# Bibliography.
natbiboptions:
    - angle
    - sectionbib
biblio-style: humannat
---
