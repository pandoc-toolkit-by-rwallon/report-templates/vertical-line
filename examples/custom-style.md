---
# Color Definitions.
color:
    - name: darkerblue
      type: RGB
      value: 0, 0, 153
    - name: darkblue
      type: RGB
      value: 0, 0, 255
    - name: blue
      type: RGB
      value: 51, 102, 255
    - name: lightblue
      type: RGB
      value: 102, 153, 255
    - name: lighterblue
      type: RGB
      value: 153, 204, 255

# Color Settings.
urlcolor: Rhodamine
citecolor: Mahogany
linkcolor: Aquamarine
section-color: darkerblue
subsection-color: darkblue
subsubsection-color: blue
paragraph-color: lightblue
subparagraph-color: lighterblue

# Font Settings.
fontfamily: libertinus
fontfamilyoptions:
    - osf
    - p

# Cover and Title Page.
notitle: true
cover: resources/cover.pdf
---
