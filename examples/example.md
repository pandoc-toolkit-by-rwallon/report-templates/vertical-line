---
# Document Configuration.
geometry: margin=2cm

# Document Metadata.
title: "Pandoc Template for Writing Reports: An Example"
title-meta: Example of a Report Built with Pandoc
subtitle: Release 0.1.0
date: January 22, 2020
author: Romain Wallon
institute: Pandoc Toolkit
keywords:
    - Pandoc
    - Template
    - Markdown
    - LaTeX
    - Report
    - Example

# Cover and Title Page.
logos:
    - resources/latex.png
    - resources/markdown.png

# Bibliography.
bibliography: resources/bibliography.bib
---

# Pandoc Template for Writing Reports: An Example

## Introduction

This document is designed as an example for a Pandoc Template used to
produce reports.
It shows the appearance of the PDF files created using this template, and
illustrates how to build them from Markdown [@markdown].

## Markdown

Indeed, the template allows to write your documents using this format, so that
you can benefit from all its features:

+ Emphasis, aka italics, with *asterisks* or _underscores_.
+ Strong emphasis, aka bold, with **asterisks** or __underscores__.
+ Combined emphasis with **asterisks and _underscores_**.
+ Strikethrough uses two tildes: ~~scratch this~~.

The above list has indeed been created using the following code.
Meanwhile, observe that it is also possible to integrate pieces of code into a
report.

```markdown
+ Emphasis, aka italics, with *asterisks* or _underscores_.
+ Strong emphasis, aka bold, with **asterisks** or __underscores__.
+ Combined emphasis with **asterisks and _underscores_**.
+ Strikethrough uses two tildes: ~~scratch this~~.
```

## Figures

Figures can also be added to your reports.
Images are put exactly at the location at which you specified them (they are
not *floating* in the document).
They are also automatically resized to better fit the pages.

![This document is written in Markdown.](resources/markdown.png)

Markdown even offers to write tables, in which you can also use the font
variants recognized by this format.

| Tables        | Are         | Cool       |
| ------------- |-------------| -----------|
| *and*         | `rendered`  | **nicely** |
| 1             | 2           | 3          |

## Building the Report

Once you have finished writing your report, you can run `pandoc` [@markdown]
using our `Makefile` to create a PDF version of your document (see the
templates' documentation for details).
Note that this software is used to create a temporary LaTeX [@latex] file
following the template you used.
So, if Markdown does not offer all the features you need, you may still write
raw \LaTeX, even in your Markdown source file(s): Pandoc will interpret it as
is.
If you do not want to use LaTeX, you may also have a look to
[Pandoc's Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), which
provides an extended set of features.

In the rest of this document, you will only find randomly generated text (aka
*Lorem Ipsum*) so as to get a report that is big enough to illustrate the full
style provided by our templates.

# Lorem Ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse ornare rhoncus felis, non euismod dolor hendrerit nec.
Sed malesuada vulputate dapibus.
Nunc et volutpat metus.
Mauris risus felis, vestibulum eget vulputate sit amet, mattis sed urna.
Nam quis lacus nec nulla sodales ornare et ut eros.
Pellentesque ante velit, consectetur at hendrerit non, blandit dictum lacus.
Nunc in porta dui.

## A Subsection

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

### A Subsubsection

Praesent ac tempus justo, ut rhoncus odio.
Cras commodo consectetur magna et ornare.
Praesent nec pharetra metus, eget convallis dolor.
In tempor pellentesque sapien, in viverra odio posuere a.
Fusce a scelerisque enim.
Integer egestas vestibulum enim quis porttitor.
Morbi sagittis tortor velit, non ullamcorper augue pellentesque eget.
Curabitur et molestie dolor.
Curabitur sed augue dolor.
Nulla feugiat est vitae vehicula placerat.
Suspendisse in condimentum lacus, id consectetur purus.
Etiam condimentum ipsum ut justo faucibus posuere.
Ut non sapien viverra, sodales eros at, semper mi.
Aenean eu lacinia ipsum.
Phasellus in consequat orci, id tristique justo.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

### A Subsubsection

Praesent ac tempus justo, ut rhoncus odio.
Cras commodo consectetur magna et ornare.
Praesent nec pharetra metus, eget convallis dolor.
In tempor pellentesque sapien, in viverra odio posuere a.
Fusce a scelerisque enim.
Integer egestas vestibulum enim quis porttitor.
Morbi sagittis tortor velit, non ullamcorper augue pellentesque eget.
Curabitur et molestie dolor.
Curabitur sed augue dolor.
Nulla feugiat est vitae vehicula placerat.
Suspendisse in condimentum lacus, id consectetur purus.
Etiam condimentum ipsum ut justo faucibus posuere.
Ut non sapien viverra, sodales eros at, semper mi.
Aenean eu lacinia ipsum.
Phasellus in consequat orci, id tristique justo.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

## A Subsection

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

### A Subsubsection

Praesent ac tempus justo, ut rhoncus odio.
Cras commodo consectetur magna et ornare.
Praesent nec pharetra metus, eget convallis dolor.
In tempor pellentesque sapien, in viverra odio posuere a.
Fusce a scelerisque enim.
Integer egestas vestibulum enim quis porttitor.
Morbi sagittis tortor velit, non ullamcorper augue pellentesque eget.
Curabitur et molestie dolor.
Curabitur sed augue dolor.
Nulla feugiat est vitae vehicula placerat.
Suspendisse in condimentum lacus, id consectetur purus.
Etiam condimentum ipsum ut justo faucibus posuere.
Ut non sapien viverra, sodales eros at, semper mi.
Aenean eu lacinia ipsum.
Phasellus in consequat orci, id tristique justo.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

### A Subsubsection

Praesent ac tempus justo, ut rhoncus odio.
Cras commodo consectetur magna et ornare.
Praesent nec pharetra metus, eget convallis dolor.
In tempor pellentesque sapien, in viverra odio posuere a.
Fusce a scelerisque enim.
Integer egestas vestibulum enim quis porttitor.
Morbi sagittis tortor velit, non ullamcorper augue pellentesque eget.
Curabitur et molestie dolor.
Curabitur sed augue dolor.
Nulla feugiat est vitae vehicula placerat.
Suspendisse in condimentum lacus, id consectetur purus.
Etiam condimentum ipsum ut justo faucibus posuere.
Ut non sapien viverra, sodales eros at, semper mi.
Aenean eu lacinia ipsum.
Phasellus in consequat orci, id tristique justo.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.

#### A Paragraph

Mauris placerat massa nec erat dapibus finibus vel ut mauris.
Nulla a venenatis risus.
Donec blandit lorem ullamcorper dui elementum, non facilisis mauris dictum.
Proin augue nulla, iaculis vitae tincidunt ac, pharetra vel arcu.
In dui tellus, accumsan sit amet suscipit vitae, cursus a neque.
Sed vitae euismod dui.
Quisque gravida hendrerit efficitur.
Integer aliquam quam eu sapien fringilla commodo.
Praesent sit amet condimentum odio, egestas volutpat urna.
Aenean a augue nec erat accumsan porta vel id risus.
Maecenas sem odio, dapibus ac ipsum ac, venenatis elementum dui.
Cras convallis eget erat sed posuere.
Phasellus tincidunt aliquam volutpat.
Aenean a maximus metus.
Donec id augue bibendum sapien pellentesque vestibulum ac vitae felis.
Praesent mattis molestie neque quis fringilla.

##### A Subparagraph

Nam non purus lacus.
Nam sapien mauris, maximus a turpis ac, pharetra pharetra odio.
Integer sed nisl quis dolor egestas interdum.
Duis purus ante, bibendum sit amet sapien non, fermentum finibus tellus.
Quisque non euismod nibh, a porttitor mi.
Aenean consequat turpis nec sodales rutrum.
Maecenas mattis dolor urna, ut malesuada ante lobortis vel.
Sed commodo purus varius eros molestie aliquam.
Praesent mattis consequat mi, vitae sollicitudin justo venenatis a.
Nulla commodo turpis arcu, sit amet fermentum felis feugiat tincidunt.
Vestibulum semper ornare velit, ut consequat odio fermentum a.
Etiam ipsum odio, mattis nec felis nec, faucibus finibus quam.
Ut posuere turpis id nibh pharetra, mollis luctus velit consectetur.
In in purus in mauris pellentesque bibendum sit amet quis mauris.
Maecenas accumsan nibh quam, eget pellentesque nisi consectetur eu.

##### A Subparagraph

Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus.
Cras commodo malesuada turpis sed accumsan.
Suspendisse ac luctus nibh.
Sed scelerisque sem quam, id porttitor ex blandit at.
Nulla ultricies dignissim ante eget fringilla.
Maecenas ut ex justo.
Quisque eget imperdiet augue, vel condimentum magna.
Donec scelerisque, dolor in vestibulum dignissim, dui urna viverra sapien, non
congue massa justo eu lacus.
Proin suscipit blandit arcu nec ultrices.
Integer faucibus nisi est, vel posuere augue placerat eu.

##### A Subparagraph

In hac habitasse platea dictumst.
Aliquam lacinia lectus vel quam congue, ut tempus magna pharetra.
Nunc semper nisi quis lacinia dapibus.
Quisque sit amet velit odio.
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac
turpis egestas.
Sed sed felis ullamcorper, dapibus orci vel, pretium urna.
Praesent ut vehicula sem.
Praesent bibendum leo eu ornare dictum.
Etiam ac enim vitae eros accumsan aliquet sed vitae ligula.
Aliquam ligula risus, tempus at odio a, accumsan finibus est.
Vivamus purus nulla, blandit vel malesuada ac, commodo sed tellus.
Praesent mattis sem placerat, dapibus tortor eu, dictum tortor.
Sed nec lacinia neque.
Proin lobortis sapien ac dui cursus congue.
